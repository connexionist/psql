FROM alpine:3.12

ARG PSQL_VERSION=12.4-r0

RUN apk --no-cache add postgresql-client==${PSQL_VERSION}
ENTRYPOINT ["psql"]
